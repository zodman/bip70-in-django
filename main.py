from importd import d
import datetime
from proto import PaymentRequest, PaymentDetails, Payment, PaymentACK
from obelisk import bitcoin 
import obelisk
from logs import LOGGING
import random
import logging

log = logging.getLogger(__name__)

NETWORK = "test"
minutes_expired = 3
OUTPUTS = (
    (0.123, "n26DbwPgjEVe4wyWANrnpW2F6RWVTkR3dU"),
)


d(DEBUG=True,LOGGING=LOGGING)

@d("/show")
def show(request):
    output ="""
    zodman gw </br>
    <a href="bitcoin:%s?r=%s" >Paga con bitcoins</a>
    """ % ( OUTPUTS[0][1], "http://localhost:8000")
    return d.HttpResponse(output)

@d("/payment")
def payment(request):
    data = request.body
    payment_ack = PaymentACK()

    payment = Payment()
    payment.ParseFromString(data)

    log.info("get data %s" % payment.merchant_data)
    payment_ack.payment.CopyFrom(payment)
    payment_ack.memo = "Zodman te agradece!!! su pago fue realizado, gracias"

    response =  d.HttpResponse()
    response.write(payment_ack.SerializeToString())
    return response

@d("/")
def index(request):
    p = PaymentDetails()
    p.network = NETWORK

    now = datetime.datetime.now()
    expired_in = now + datetime.timedelta(minutes=minutes_expired)
    p.memo = "bip70 implementation"
    p.time = int(expired_in.strftime("%s"))
    p.payment_url = "http://localhost:8000/payment"
    p.merchant_data = "transaction-%s" % random.random()
    for m, address in OUTPUTS:
        output_script = bitcoin.output_script(address)
        output = p.outputs.add()
        output.script = output_script
        output.amount = int(m*int('1'+'0'*5))
    response = d.HttpResponse(mimetype="application/bitcoin-paymentrequest")
    data = p.SerializeToString()
    payment_request = PaymentRequest()
    payment_request.serialized_payment_details = data
    response.write(payment_request.SerializeToString())
    return response

if __name__ == "__main__":

    obelisk.select_network("testnet")
    print "gnome-open 'bitcoin:%s?r=%s'" % ( OUTPUTS[0][1], "http://localhost:8000")
    d.main()
